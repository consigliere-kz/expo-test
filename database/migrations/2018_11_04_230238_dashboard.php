<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64)->comment('Название Dashboard\'а');
        });

        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias', 64)->comment('Служебное название');
            $table->string('name', 64)->comment('Название');
            $table->string('expression', 64)->comment('Выражение для вычисления');
        });

        Schema::create('dashboard_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dashboard_id')->comment('Идентификатор dashboard\'a');
            $table->unsignedInteger('metric_id')->comment('Метрика для отображения');
            $table->unsignedInteger('order')->comment('Место отображения блока в шаблоне');
            $table->string('view_type', 32)->comment('Тип отображения');
            $table->string('chart_type', 32)
                ->comment('Тип графика')
                ->nullable(true);
            $table->unsignedInteger('period_id')
                ->comment('Периодичность данных')
                ->nullable(true);

            $table->foreign('dashboard_id', 'fk-dashboard_elements-dashboard')
                ->references('id')
                ->on('dashboards')
                ->onDelete('CASCADE');
            $table->foreign('metric_id', 'fk-dashboard_elements-metric')
                ->references('id')
                ->on('metrics')
                ->onDelete('CASCADE');
            $table->foreign('period_id', 'fk-dashboard_elements-period')
                ->references('id')
                ->on('periods')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_elements');
        Schema::dropIfExists('dashboards');
        Schema::dropIfExists('periods');
    }
}
