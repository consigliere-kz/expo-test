<?php

use App\Models\Resource;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class TestableMetricsSeeder
 */
class TestableMetricsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weatherResourceId = DB::table('resources')->insertGetId([
            'type' => Resource::TYPE_HTTP,
            'content_type' => Resource::CONTENT_TYPE_JSON,
            'path' => 'http://api.openweathermap.org/data/2.5/weather?q=Novosibirsk,ru&appid=d3a3b7112a5a88a03c78ca53b6b91c82&units=metric'
        ]);
        DB::table('metrics')->insert([
            'name' => 'Температура в Новосибирске',
            'resource_id' => $weatherResourceId,
            'resource_value' => 'main.temp'
        ]);
        DB::table('metrics')->insert([
            'name' => 'Влажность в Новосибирске',
            'resource_id' => $weatherResourceId,
            'resource_value' => 'main.humidity'
        ]);

        $currencyRateResourceId = DB::table('resources')->insertGetId([
            'type' => Resource::TYPE_HTTP,
            'content_type' => Resource::CONTENT_TYPE_XML,
            'path' => 'http://www.cbr.ru/scripts/XML_daily.asp'
        ]);
        DB::table('metrics')->insert([
            'name' => 'Курс евро',
            'resource_id' => $currencyRateResourceId,
            'resource_value' => "//Valute[@ID='R01239']/Value"
        ]);
        DB::table('metrics')->insert([
            'name' => 'Курс доллара США',
            'resource_id' => $currencyRateResourceId,
            'resource_value' => "//Valute[@ID='R01235']/Value"
        ]);
    }
}
