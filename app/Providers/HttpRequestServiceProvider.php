<?php

declare(strict_types=1);

namespace App\Providers;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Illuminate\Support\ServiceProvider;

/**
 * Class HttpRequestServiceProvider
 * @package App\Providers
 */
class HttpRequestServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            HttpClient::class,
            \Http\Adapter\Guzzle6\Client::class
        );
        $this->app->bind(
            MessageFactory::class,
            MessageFactory\GuzzleMessageFactory::class
        );
    }
}