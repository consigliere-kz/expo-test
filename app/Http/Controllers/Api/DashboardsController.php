<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreDashboard;
use App\Http\Resources\DashboardResource;
use App\Models\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class DashboardsController
 * @package App\Http\Controllers\Api
 */
class DashboardsController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return DashboardResource::collection(Dashboard::all());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return Dashboard::find($id);
    }

    /**
     * @param StoreDashboard $request
     * @return DashboardResource
     */
    public function store(StoreDashboard $request)
    {
        $dashboard = Dashboard::create($request->all());
        
        return new DashboardResource($dashboard);
    }

    /**
     * @param StoreDashboard $request
     * @param $id
     * @return DashboardResource
     */
    public function update(StoreDashboard $request, $id)
    {
        $dashboard = Dashboard::findOrFail($id);
        $dashboard->update($request->all());

        return new DashboardResource($dashboard);
    }

    /**
     * @param Request $request
     * @param $id
     * @return int
     */
    public function destroy(Request $request, $id)
    {
        $dashboard = Dashboard::findOrFail($id);
        $dashboard->delete();

        return 204;
    }
}
