<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreDashboardElement;
use App\Http\Resources\DashboardElementResource;
use App\Models\DashboardElement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class DashboardElementsController
 * @package App\Http\Controllers\Api
 */
class DashboardsElementsController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return DashboardElementResource::collection(DashboardElement::all());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return DashboardElement::find($id);
    }

    /**
     * @param StoreDashboardElement $request
     * @return DashboardElementResource
     */
    public function store(StoreDashboardElement $request)
    {
        $dashboardElement = DashboardElement::create($request->all());

        return new DashboardElementResource($dashboardElement);
    }

    /**
     * @param StoreDashboardElement $request
     * @param $id
     * @return DashboardElementResource
     */
    public function update(StoreDashboardElement $request, $id)
    {
        $dashboardElement = DashboardElement::findOrFail($id);
        $dashboardElement->update($request->all());

        return new DashboardElementResource($dashboardElement);
    }

    /**
     * @param Request $request
     * @param $id
     * @return int
     */
    public function destroy(Request $request, $id)
    {
        $dashboardElement = DashboardElement::findOrFail($id);
        $dashboardElement->delete();

        return 204;
    }
}
