<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Metric
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property int $resource_id
 * @property string $resource_value
 *
 * @property-read \App\Models\Resource $resource
 * @property-read \App\Models\MetricValue[] $values
 */
class Metric extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(MetricValue::class);
    }
}
