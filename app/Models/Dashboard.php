<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dashboard
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 */
class Dashboard extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];
}
