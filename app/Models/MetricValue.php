<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MetricValue
 * @package App\Models
 *
 * @property int $id
 * @property int $metric_id
 * @property float $value
 * @property string $gathered_at
 */
class MetricValue extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'metrics_values';
}
