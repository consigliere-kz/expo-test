<?php

declare(strict_types=1);

namespace App\lib\Parsers;

/**
 * Такой себе парсер)
 *
 * Class RawValueParser
 * @package App\lib\Parsers
 */
class RawValueParser implements ParserInterface
{
    /**
     * @inheritdoc
     */
    public function getValue(string $data, string $path = '')
    {
        return $data;
    }
}