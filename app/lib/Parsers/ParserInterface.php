<?php

declare(strict_types=1);

namespace App\lib\Parsers;

/**
 * Interface ParserInterface
 * @package App\lib\Parsers
 */
interface ParserInterface
{
    /**
     * @param string $data
     * @param string $path
     * @return mixed
     */
    public function getValue(string $data, string $path);
}