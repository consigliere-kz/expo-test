<?php

declare(strict_types=1);

namespace App\lib\Parsers;

/**
 * Class ParserFactory
 * @package App\lib\Parsers
 */
class ParserFactory
{
    /**
     * @param string $name
     * @return ParserInterface
     */
    public function createParser(string $name): ParserInterface
    {
        $className = ucfirst($name) . 'Parser';
        $baseClass = "App\\lib\\Parsers\\{$className}";

        try {
            $parser = resolve($baseClass);
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Can't resolve parser with name {$name}");
        }

        return $parser;
    }
}