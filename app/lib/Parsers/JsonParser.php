<?php

declare(strict_types=1);

namespace App\lib\Parsers;

/**
 * Class JsonParser
 * @package App\lib\Parsers
 */
class JsonParser implements ParserInterface
{
    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function getValue(string $data, string $path)
    {
        $array = $this->parseJson($data);

        if (!array_has($array, $path)) {
            throw new \Exception("Json doesn't contain specified path: {$path}");
        }

        return array_get($array, $path);
    }

    /**
     * @param string $data
     * @return array
     * @throws \Exception
     */
    protected function parseJson(string $data): array
    {
        $array = json_decode($data, true);
        $error = json_last_error();

        if ($error !== JSON_ERROR_NONE) {
            throw new \Exception("Invalid json string. ErrCode: {$error}");
        }

        return $array;
    }
}