<?php

declare(strict_types=1);

namespace App\lib\Parsers;

/**
 * Class XmlParser
 * @package App\lib\Parsers
 */
class XmlParser implements ParserInterface
{
    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function getValue(string $data, string $path)
    {
        $xml = simplexml_load_string($data);
        $node = $xml->xpath($path);

        if ($node === false) {
            throw new \Exception("Xml doesn't contain specified path: {$path}");
        }

        return (string)$node[0];
    }
}