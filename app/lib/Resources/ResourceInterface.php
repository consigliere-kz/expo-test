<?php

declare(strict_types=1);

namespace App\lib\Resources;

/**
 * Interface ResourceInterface
 * @package App\lib\Resources
 */
interface ResourceInterface
{
    /**
     * @return string
     */
    public function getData(): string;
}