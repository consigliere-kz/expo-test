<?php

declare(strict_types=1);

namespace App\lib\Resources;

/**
 * Class AbstractLocatedResource
 * @package App\lib\Resources
 */
trait LocatedResource
{
    protected $path = '';

    /**
     * @param string $path
     * @return $this
     */
    public function setPath(string $path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws \InvalidArgumentException
     * @return void
     */
    protected function ensurePathIsDefined(): void
    {
        if (!$this->path) {
            throw new \InvalidArgumentException('Resource path is required for ' . static::class);
        }
    }
}