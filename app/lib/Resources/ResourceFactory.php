<?php

declare(strict_types=1);

namespace App\lib\Resources;

/**
 * Class ResourceFactory
 * @package App\lib\Resources
 */
class ResourceFactory
{
    /**
     * @param string $name
     * @return ResourceInterface
     */
    public function createResource(string $name): ResourceInterface
    {
        $className = ucfirst($name) . 'Resource';
        $baseClass = "App\\lib\\Resources\\{$className}";

        try {
            $resource = resolve($baseClass);
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Can't resolve resource with name {$name}");
        }

        return $resource;
    }

    /**
     * Не нравится этот метод.
     * Удобнее было бы передавать массив/объект $params, а не только $path. Т.к. ресурсы могут быть разные и иметь разные конфиги
     * //todo Реализовать интерфейс ConfigurableResourceInterface: setParams(), getParams()
     *
     * @param string $name
     * @param string $path
     * @return ResourceInterface
     */
    public function createLocatedResource(string $name, string $path): ResourceInterface
    {
        $resource = $this->createResource($name);

        if (!$resource instanceof LocatedResourceInterface) {
            throw new \InvalidArgumentException("Can't set path for resource with name {$name}");
        }

        $resource->setPath($path);

        return $resource;
    }
}