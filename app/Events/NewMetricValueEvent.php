<?php

namespace App\Events;

use App\Models\MetricValue;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class NewMetricValueEvent
 * @package App\Services\Events
 */
class NewMetricValueEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var MetricValue
     */
    protected $metricValue;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MetricValue $metricValue)
    {
        $this->metricValue = $metricValue;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("metrics");
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'value' => $this->metricValue->value,
            'metric_id' => $this->metricValue->metric_id,
            'date' => $this->metricValue->gathered_at,
        ];
    }
}
