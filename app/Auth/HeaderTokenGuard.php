<?php

declare(strict_types=1);

namespace App\Auth;

use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

/**
 * Пишем свой Guard для аутентификации, т.к. по ТЗ надо брать токен из заголовка Auth
 * TokenGuard не позволяет это кастомизировать и использует стандартную Bearer-аутентификацию
 *
 * Class HeaderTokenGuard
 * @package App\Auth
 */
class HeaderTokenGuard implements Guard
{
    use GuardHelpers;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * The name of the header key from the request containing the API token.
     *
     * @var string
     */
    protected $headerKey;

    /**
     * The name of the token "column" in persistent storage.
     *
     * @var string
     */
    protected $storageKey;

    /**
     * Create a new authentication guard.
     *
     * @param UserProvider $provider
     * @param Request $request
     * @param string $headerKey
     * @param  string $storageKey
     */
    public function __construct(
        UserProvider $provider,
        Request $request,
        string $headerKey = 'Auth',
        string $storageKey = 'access_token'
    )
    {
        $this->request = $request;
        $this->provider = $provider;
        $this->headerKey = $headerKey;
        $this->storageKey = $storageKey;
    }

    /**
     * @return Authenticatable|null
     */
    public function user()
    {
        if (!is_null($this->user)) {
            return $this->user;
        }

        $user = null;

        $token = $this->request->header($this->headerKey);

        if (!empty($token)) {
            $user = $this->provider->retrieveByCredentials(
                [$this->storageKey => $token]
            );
        }

        return $this->user = $user;
    }

    /**
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = []): bool
    {
        if (empty($credentials[$this->headerKey])) {
            return false;
        }

        $credentials = [$this->storageKey => $credentials[$this->headerKey]];

        if ($this->provider->retrieveByCredentials($credentials)) {
            return true;
        }

        return false;
    }
}