<?php

namespace App\Console\Commands;

use App\Services\MetricsService;
use App\Models\Metric;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

/**
 * Class GatherMetric
 * @package App\Console\Commands
 */
class GatherMetric extends Command
{
    /**
     * @var MetricsService
     */
    protected $metricService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'metrics:gather {--metricId=} {--printOnly : Don\'t save, print only.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gather metric value from specified resource';

    /**
     * Create a new command instance.
     *
     * @param MetricsService $service
     */
    public function __construct(MetricsService $service)
    {
        parent::__construct();

        $this->metricService = $service;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $metricId = (int)$this->option('metricId');
        $metric = Metric::findOrFail($metricId);
        $value = $this->metricService->getResourceValue($metric);

        if ($this->option('printOnly')) {
            $this->info("Metric '{$metric->name}' current value: {$value}\n");

            return;
        }

        if (!$this->metricService->storeValue($metric, $value)) {
            Log::channel('metrics')->warning("Can't save metric value. MetricId: {$metric->id}. Value: {$value}");
        };
    }
}
