<details><summary>Задача</summary>
<p>
Сервис получает и хранит числовые данные из различных источников.
Сервис получает и хранит  структуры по отображению этих данных. 

Пути получения данных: 
Забирать по http по крону.
В ответе может быть
число
json
html 
Авторизованное обращение на адрес сервиса

Структура по отображению:
название шаблона
массив блоков: 
Место отображение блока в шаблоне
Источник данных
Тип отображения [число, график]
Тип графика
период (для графика)
нужно чтоб понимал периоды типа 
последний час
последние 3и часа
последние сутки
и т.д


По авторизованному запросу сервис возвращает список доступных источников
По авторизованному запросу сервис возвращает запрошенную структуру
По авторизованному запросу сервис возвращает данные (с учётом периода, когда это необходимо)
Обновление эти данные через websocket
В качестве аутентификации используем ключ 123 в заголовке Auth
</p>
</details>


**Требования**

https://laravel.com/docs/5.7/installation#server-requirements

**Установка и настройка**

- git clone
- ```composer install```
- Выдать права на запись для директорий: bootstrap/cache, storage
- ```cp .env.example .env```
- ```php artisan key:generate```
- Настраиваем .env:
  1) подключение к БД
  2) BROADCAST_DRIVER  //можно оставить log, тогда все данные, которые должны уходить на веб-сокет смотрим в логах
- ```php artisan migrate```
- ```php artisan db:seed```



Проверял обновления данных через WebSocket используя redis + laravel-echo-server. Устанавливаем:
- установить redis
- ```npm install -g laravel-echo-server```
- ```laravel-echo-server start```
- Пишем в ```.env```: BROADCAST_DRIVER=redis

**Как слушать broadcast с фронта:**
  - ```npm install``` 
  
```javascript
import Echo from 'laravel-echo'

window.io = require('socket.io-client');
window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

window.Echo.channel('metrics')
    .listen('NewMetricValueEvent', (e) => {
        console.log(e);
    });
```

**Http-ресурсы(Не забываем про заголовок Auth: 123):**
- GET /api/metrics - список доступных метрик вместе с источником данных
- GET /api/metrics/{id} - информация по одной метрике
- GET /api/metrics/{id}/values - значения метрики. Принимает параметры from, to или period(см periods.alias). Например:
  - GET /api/metrics/{id}/values?from=2018-11-05
  - GET /api/metrics/{id}/values?from=2018-11-02&to=2018-11-05
  - GET /api/metrics/{id}/values?period=lastHour
- POST /api/metrics/{id}/values - добавление значения метрики в БД. Значение "value" обязательное, Если это Ajax-запрос, то вернёт ошибки при неправильном заполнении.
- REST /api/dashboards
- REST /api/dashboards/elements

**Консольные команды:**
 - Сбор метрик: ```php artisan metrics:gather --metricId=2``` //опционально можно указать флаг --printOnly, тогда будет вывод в консоль, без сохранения значения в БД

**По коду:**

- ```App\Auth\HeaderTokenGuard``` - Свой Guard для аутентификации, т.к. по ТЗ надо брать токен из заголовка Auth. 
Встроенный TokenGuard не позволяет это кастомизировать и использует стандартную Bearer-аутентификацию, хотя я бы лучше взял его.
- ```App\Services``` - Содержит сервис для работы с метриками
- ```App\lib\Parsers``` - парсеры данных
- ```App\lib\Resources``` - источники данных(реализован только HttpResource)
- ```App\Http\Requests``` - Валидация запросов к API

**TODO:**
- App\Http\Controllers\Api\MetricsController - методы работы со значениями(values) вынести в другой контроллер.

- БД поле resources.path - лучше заменить на JSON поле с параметрами ресурса, т.к. для того же HttpResource может потребоваться задавать не только путь, но и тип запроса, заголовки и другие параметры

- БД поле metrics.resource_value - Не нравится, что значение зависит от content-type(для xml - xpath, json - через разделитель с точкой). Вижу пару вариантов решения:
  - Приводить к одному виду и  писать интерфейс с которым сможет работать ResourceParser. Т.е. будет такая сигнатура Parser::getValue(AttributePath $path)
  - Для каждой метрики задается собственный парсер значений с методом getValue(), напр-р CbrCurrencyDollarParser;
- БД поля dashboard_element.chart_type, dashboard_element.interval_id избыточны, т.к. нужны только для графиков. Тоже лучше сделать json поле view_params, но будет чуть сложнее валидировать.

**Литература:**
- https://laravel.com/docs/5.7/
- https://www.toptal.com/laravel/restful-laravel-api-tutorial
- https://code.tutsplus.com/ru/tutorials/how-to-create-a-custom-authentication-guard-in-laravel--cms-29667
- https://medium.com/@dennissmink/laravel-echo-server-how-to-24d5778ece8b
- https://laravel.io/forum/02-13-2014-i-can-not-get-inputs-from-a-putpatch-request - Оказалось laravel не понимает PUT запросы(